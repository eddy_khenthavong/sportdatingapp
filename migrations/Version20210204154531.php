<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210204154531 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pratique ADD sport_id INT NOT NULL');
        $this->addSql('ALTER TABLE pratique ADD CONSTRAINT FK_1F2B781AC78BCF8 FOREIGN KEY (sport_id) REFERENCES sport (id)');
        $this->addSql('CREATE INDEX IDX_1F2B781AC78BCF8 ON pratique (sport_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1A85EFD2CD4F5A30 ON sport (design)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pratique DROP FOREIGN KEY FK_1F2B781AC78BCF8');
        $this->addSql('DROP INDEX IDX_1F2B781AC78BCF8 ON pratique');
        $this->addSql('ALTER TABLE pratique DROP sport_id');
        $this->addSql('DROP INDEX UNIQ_1A85EFD2CD4F5A30 ON sport');
    }
}
