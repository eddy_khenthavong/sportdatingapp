<?php

namespace App\Controller;

use App\Entity\Sport;
use App\Form\SportType;
use App\Form\SportType2;
use App\Repository\SportRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/sport")
 */
class SportController extends AbstractController
{
    /**
     * @Route("/", name="sport_index", methods={"GET"})
     */
    public function index(SportRepository $sportRepository): Response
    {
        return $this->render('sport/index.html.twig', [
            'sports' => $sportRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sport_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sport = new Sport();
        $form = $this->createForm(SportType::class, $sport);
        // $form->handleRequest($request);

        $sport2 = new Sport();
        $form2 = $this->createForm(SportType2::class, $sport2);
        $form2->handleRequest($request);

        $anthoy = $this->getDoctrine()->getRepository(Sport::class)->findOneBy(['design' => $form2 ->getData()->getDesign()]);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sport);
            $entityManager->flush();

            return $this->redirectToRoute('pratique_new');
        }
        else if ($form2->isSubmitted() && $form2->isValid() && !$anthoy) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sport2);
            $entityManager->flush();

            return $this->redirectToRoute('pratique_new');
        }
        else if ($anthoy) {
            print "Come On !! Ce sport est déjà connu";
        }

        return $this->render('sport/new.html.twig', [
            'sport' => $sport,
            'form' => $form->createView(),
            'form2' => $form2->createView(),
        ]);
}

    /**
     * @Route("/{id}", name="sport_show", methods={"GET"})
     */
    public function show(Sport $sport): Response
    {
        return $this->render('sport/show.html.twig', [
            'sport' => $sport,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sport_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sport $sport): Response
    {
        $form = $this->createForm(SportType::class, $sport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sport_index');
        }

        return $this->render('sport/edit.html.twig', [
            'sport' => $sport,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sport_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Sport $sport): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sport->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sport);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sport_index');
    }
}
