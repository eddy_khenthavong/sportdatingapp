<?php

namespace App\Controller;

use App\Entity\Sport;
use App\Entity\Pratique;
use App\Form\PratiqueType;
use App\Repository\PratiqueRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/pratique")
 */
class PratiqueController extends AbstractController
{
    /**
     * @Route("/", name="pratique_index", methods={"GET"})
     */
    public function index(PratiqueRepository $pratiqueRepository): Response
    {
        return $this->render('pratique/index.html.twig', [
            'pratiques' => $pratiqueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pratique_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pratique = new Pratique();
        $form = $this->createForm(PratiqueType::class, $pratique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $anthoy = $this->getDoctrine()->getRepository(Sport::class)->findOneBy(['design' => $form->get('sport')->getData()->getDesign()]);
                // goal => get id set to pratique table 
            $pratique->setSport($anthoy);
            $entityManager->persist($pratique);
            $entityManager->flush();

            return $this->redirectToRoute('pratique_index');
        }

        return $this->render('pratique/new.html.twig', [
            'pratique' => $pratique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pratique_show", methods={"GET"})
     */
    public function show(Pratique $pratique): Response
    {
        return $this->render('pratique/show.html.twig', [
            'pratique' => $pratique,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pratique_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pratique $pratique): Response
    {
        $form = $this->createForm(PratiqueType::class, $pratique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pratique_index');
        }

        return $this->render('pratique/edit.html.twig', [
            'pratique' => $pratique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pratique_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pratique $pratique): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pratique->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pratique);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pratique_index');
    }
}
