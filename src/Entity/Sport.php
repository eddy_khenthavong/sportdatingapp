<?php

namespace App\Entity;

use App\Entity\Pratique;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SportRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SportRepository::class)
 */

class Sport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="([0-9\-\_]+)",
     *     match=false,
     *     message="Le sport ne peut pas contenir de chiffre."
     * )
     * @Assert\NotBlank(
     *     message="La valeur ne peut pas être vide.")
     */
    private $design;

    /**
     * @ORM\OneToMany(targetEntity=Pratique::class, mappedBy="sport", orphanRemoval=true)
     */
    private $pratiques;

    public function __construct()
    {
        $this->pratiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->design;
    }

    public function getDesign(): ?string
    {
        return $this->design;
    }

    public function setDesign(string $design): self
    {
        $this->design = $design;

        return $this;
    }

    /**
     * @return Collection|Pratique[]
     */
    public function getPratiques(): Collection
    {
        return $this->pratiques;
    }

    public function addPratique(Pratique $pratique): self
    {
        if (!$this->pratiques->contains($pratique)) {
            $this->pratiques[] = $pratique;
            $pratique->setSport($this);
        }

        return $this;
    }

    public function removePratique(Pratique $pratique): self
    {
        if ($this->pratiques->removeElement($pratique)) {
            // set the owning side to null (unless already changed)
            if ($pratique->getSport() === $this) {
                $pratique->setSport(null);
            }
        }

        return $this;
    }
}
